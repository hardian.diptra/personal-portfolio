from django.shortcuts import render
from .models import Projects

# Create your views here.
def home(request):
    myprojects = Projects.objects.all()
    return render(request, 'portfolio/home.html', {'myprojects':myprojects})