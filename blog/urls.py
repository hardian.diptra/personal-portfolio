from django.urls import path
from . import views

# menggunakan app_name untuk mendefinisikan nama app nya agar saat ada nama file yang sama maka akan mencari sesuai dengan nama app nya
app_name = 'blog'

urlpatterns = [
    path('', views.all_blogs, name='all_blogs'),
    path('<int:blog_id>/', views.detail, name='detail'),
]