from django.db import models

# Create your models here.
class Projects(models.Model):
    title = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    photo = models.ImageField(upload_to='portfolio/images/')
    links = models.URLField(blank=True)

    def __str__(self):
        return self.title